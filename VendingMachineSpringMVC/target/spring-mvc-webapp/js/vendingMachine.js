var money = 0.00;
var itemId;
$(document).ready(function() {
    loadItems();
});

function loadItems(money) {
    clearItemTable();
    itemId = 0;

    // grab the the tbody element that will hold the rows of item information
    $.ajax({
            type: 'GET',
            url: 'http://localhost:8080/VendingMachineSpringMVC/getItems',
            success: function(data, status) {
                $.each(data, function(index, item) {
                    var id = item.id;
                    var name = item.name;
                    var price = item.price;
                    var quantity = item.quantity;
                    $("#button" + id).on('click', function() {
                        $('#itemNum').text(id);
                        itemId += id;
                        money -= price;
                    });
                });
            },
            error: function() {
                $('#errorMessages')
                    .append($('<li >')
                            .attr({
                                class: 'list-group-item list-group-item-danger'
                            })
                            .text('Error calling web service.  Please try again later.'));
                    }
            });
    }

    function clearItemTable() {
        $('#contentRows').empty();
    }
    function clearChangeTable() {
      $('#changeRows').empty();
    }

    function clearMessage() {
      $('#message').empty();
    }

    $('#returnChange').click(function() {
      $('#money').replaceWith('<h2 id=money>$0.00</h2>');
      money -= money;

    });
    $('#dollar').click(function() {
        money += 1.00;
        $('#money').text('$' + money.toFixed(2));
    });
    $('#nickel').click(function() {
        money += 0.05;
        $('#money').text('$' + money.toFixed(2));
    });
    $('#dime').click(function() {
        money += 0.10;
        $('#money').text('$' + money.toFixed(2));
    });
    $('#quarter').click(function() {
        money += 0.25;
        $('#money').text('$' + money.toFixed(2));
    });

    $('#purchase').click(function() {

      makePurchase(itemId, money);

    });

    function makePurchase(id, money) {
        clearMessage();
        clearChangeTable();
        $.ajax({
                    type: 'GET',
                    url: 'http://localhost:8080/money/makePurchase',
                    success: function(data, status) {

                                        var quarters = data.quarters;
                                        var dimes = data.dimes;
                                        var nickels = data.nickels;
                                        var dollars = data.dollars;


                                        var changeRows = $('#changeRows');
                                        var row = '<tr>';
                                        row += '<td>' + ' ' + quarters + ' ' + 'quarters ' + '</td>';
                                        row += ' <td>' + ' ' + dimes + ' ' + 'dimes' + ' ' + '</td>';
                                        row += ' <td>' + ' ' + nickels + ' ' + 'nickels' + ' ' + '</td>';
                                        row += ' <td>' + ' ' + dollars + ' ' + 'dollars' + ' ' + '</td>';
                                        row += '</tr>';
                                        changeRows.append(row);
                                        $('#message').empty();
                                        $('#message').text('Thank You!');
                                        loadItems();
                                      },
                                      error: function(xhr, status, error) {

                                        var err = JSON.parse(xhr.responseText);
                                          var message = err.message;
                                          $('#message').text(message);
                                          loadItems();
                                              }
                                    });
                                  }