<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Vending Machine</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1>
                <center>Vending Machine</center>
            </h1>
            <hr/>

            <div class="col-md-9">
                <div row>
                    <table id="itemTable" class="table table-hover">
                        <c:forEach var="currentItem" items="${itemList}">
                            <button id="button<c:out value="${currentItem.id}"/>" style = "height:200px;width:250px"type = "button">
                                <p>
                                    <c:out value="${currentItem.id}"/>
                                </p>
                                <p>
                                    <c:out value="${currentItem.name}"/>
                                </p>
                                <p>
                                    $<c:out value="${currentItem.price}"/>
                                </p>
                                <p>
                                    Quantity: <c:out value="${currentItem.quantity}"/>
                                </p>
                            </button>
                        </c:forEach>
                    </table>
                </div>
            </div>
            <div class="col-md-3">
                <h1>Total $ in</h1>
                <div>
                    <h2 id=money>$0.00</h2>
                </div>
                <form>
                    <div class="row">
                        <button id="nickel" type="button">Nickel</button>
                        <button id="dime" type="button">Dime</button>
                    </div>
                    <div class="row">
                        <button id="quarter" type="button">Quarter</button>
                        <button id="dollar" type="button">Dollar</button>
                    </div>
                </form>
                <hr/>
                <h1>Messages</h1>
                <div>
                    <h2 id="message">Pick an item</h2>


                    <h3>Item: <span id ="itemNum"></span></h3>


                    <form>
                        <button id="purchase"> Make Purchase</button>
                    </form>
                    <hr/>
                    <h1>Change</h1>
                    <div>
                        <table id="change">
                            <tbody id="changeRows"></tbody>
                        </table>
                    </div>
                    <form>
                        <button id="returnChange" type="button"> Change Return</button>
                    </form>
                </div>
            </div>
        </div>

        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/vendingMachine.js"></script>
    </body>
</html>

