/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.controller;

import com.sg.vendingmachinespringmvc.dao.VendingMachineDao;
import com.sg.vendingmachinespringmvc.model.Item;
import com.sg.vendingmachinespringmvc.model.Money;
import java.math.BigDecimal;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller
public class ItemController {

    VendingMachineDao dao;
    Money money;
    

    @Inject
    public ItemController(VendingMachineDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {

        List<Item> itemList = dao.getAllItems();

        model.addAttribute("itemList", itemList);
        return "items";
    }

    @RequestMapping(value = "/getItems", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List getItem() {
        List<Item> itemList = dao.getAllItems();
        return itemList;
    }

    @RequestMapping(value = "/makePurchase", method = RequestMethod.PUT)
    public String getMoney(HttpServletRequest request) {

        BigDecimal getMoney = new BigDecimal(request.getParameter("money"));
        Long itemId = Long.parseLong(request.getParameter("itemId"));
        dao.updateItem(itemId, getMoney);
   
        return "redirect:/";
    }

    @RequestMapping(value = "/returnChange", method = RequestMethod.GET)
    public String returnChange(Model model) {
        int quarter = money.getQuarter();
        int dime = money.getDime();
        int nickel = money.getNickel();
        int penny = money.getPenny();
        
        model.addAttribute("quarter", quarter);
        model.addAttribute("dime", dime);
        model.addAttribute("nickel", nickel);
        model.addAttribute("penny", penny);
        
        return "change";
    }

}
