/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.dao;

import com.sg.vendingmachinespringmvc.model.Item;
import com.sg.vendingmachinespringmvc.model.Money;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class VendingMachineDaoInMemImpl implements VendingMachineDao {

    
    private Map<Long, Item> itemMap = new HashMap<>();
    Money money;

    public VendingMachineDaoInMemImpl() {
 
        

        Item item1 = new Item();
        item1.setId(1);
        item1.setName("Snickers");
        item1.setPrice(1.85);
        item1.setQuantity(9);
        itemMap.put(item1.getId(), item1);

        Item item2 = new Item();
        item2.setId(2);
        item2.setName("M & Ms");
        item2.setPrice(1.50);
        item2.setQuantity(2);
        itemMap.put(item2.getId(), item2);

        Item item3 = new Item();
        item3.setId(3);
        item3.setName("Pringles");
        item3.setPrice(2.10);
        item3.setQuantity(5);
        itemMap.put(item3.getId(), item3);

        Item item4 = new Item();
        item4.setId(4);
        item4.setName("Reese's");
        item4.setPrice(1.85);
        item4.setQuantity(4);
        itemMap.put(item4.getId(), item4);

        Item item5 = new Item();
        item5.setId(5);
        item5.setName("Pretzels");
        item5.setPrice(1.25);
        item5.setQuantity(9);
        itemMap.put(item5.getId(), item5);

        Item item6 = new Item();
        item6.setId(6);
        item6.setName("Twinkies");
        item6.setPrice(1.95);
        item6.setQuantity(3);
        itemMap.put(item6.getId(), item6);

        Item item7 = new Item();
        item7.setId(7);
        item7.setName("Doritos");
        item7.setPrice(1.75);
        item7.setQuantity(11);
        itemMap.put(item7.getId(), item7);

        Item item8 = new Item();
        item8.setId(8);
        item8.setName("Almond Joy");
        item8.setPrice(1.85);
        item8.setQuantity(0);
        itemMap.put(item8.getId(), item8);

        Item item9 = new Item();
        item9.setId(9);
        item9.setName("Trident");
        item9.setPrice(1.95);
        item9.setQuantity(6);
        itemMap.put(item9.getId(), item9);
    }

    @Override
    public List<Item> getAllItems() {
        Collection<Item> i = itemMap.values();
        return new ArrayList(i);
    }

    @Override
    public Item getItemById(Long id) {
        return itemMap.get(id);
    }

    @Override
    public Item updateItem(Long id, BigDecimal cash) {

        Item updatedItem = itemMap.get(id);
        int currentQuant = updatedItem.getQuantity();
        double price = updatedItem.getPrice();
        BigDecimal cost = new BigDecimal(price);
        BigDecimal change = cash.subtract(cost);
        currentQuant -=1;
        money.setChange(change);
        updatedItem.setQuantity(currentQuant);
        itemMap.put(updatedItem.getId(), updatedItem);

        return updatedItem;
    }



}
