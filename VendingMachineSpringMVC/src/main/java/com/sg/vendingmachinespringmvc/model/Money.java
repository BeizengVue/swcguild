/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.model;

import java.math.BigDecimal;

/**
 *
 * @author apprentice
 */
public class Money {

    private int quarter;
    private int dime;
    private int nickel;
    private int penny;

    public int getQuarter() {
        return quarter;
    }

    public int getDime() {
        return dime;
    }

    public int getNickel() {
        return nickel;
    }

    public int getPenny() {
        return penny;
    }

    public Money() {
        quarter = 0;
        dime = 0;
        nickel = 0;
        penny = 0;
    }

    public Money(BigDecimal amount) {
        setChange(amount);
    }

    public void setChange(BigDecimal amount) {
        int amountP = (int) (amount.floatValue() * 100);
        quarter = amountP / 25;
        amountP = amountP % 25;
        dime = amountP / 10;
        amountP = amountP % 10;
        nickel = amountP / 5;
        amountP = amountP % 5;
        penny = amountP / 1;
        amountP = amountP % 1;

    }
}
