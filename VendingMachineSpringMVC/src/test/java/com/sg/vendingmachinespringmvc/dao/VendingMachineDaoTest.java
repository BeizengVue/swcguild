/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.dao;

import com.sg.vendingmachinespringmvc.model.Item;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class VendingMachineDaoTest {

    private VendingMachineDao dao;

    public VendingMachineDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("vendingMachineDao", VendingMachineDao.class);

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of updateItem method, of class VendingMachineDao.
     */
    @Test
    public void testUpdateItem() {
        
//        Item ni = new Item();
//        ni.setId(11);
//        ni.setName("Candy");
//        ni.setPrice(2.25);
//        ni.setQuantity(10);
//        dao.updateItem(ni.getId());
//        Item fromDb = dao.getItemById((ni.getId()));
//        assertEquals(fromDb, ni);
    }

    /**
     * Test of getAllItems method, of class VendingMachineDao.
     */
    @Test
    public void testGetAllItems() { 
        
        List<Item> iList = dao.getAllItems();
        assertEquals(iList.size(), 9);
        
    }

    /**
     * Test of getItemById method, of class VendingMachineDao.
     */
    @Test
    public void testGetItemById() {
        
//        Item ni = new Item();
//        ni.setId(11);
//        ni.setName("Candy");
//        ni.setPrice(2.25);
//        ni.setQuantity(10);
//        dao.updateItem(ni.getId());
//        
//        Item fromDb = dao.getItemById(ni.getId());
//        assertEquals(fromDb, ni);
    }



}
